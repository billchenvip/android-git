package com.example.myrecieverapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver2 extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        String S =intent.getAction();
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (S=="For_Help")
        {

            String str =intent.getStringExtra("name");
            Log.i("Message",str+" For_Help"+"MyReceiver2");
            abortBroadcast();//在接收者2这里截停广播
        }
    }
}
