package com.example.billchentest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {
    //定义界面上的控件变量
    private RadioGroup rdg_MyGroup;
    private RadioButton rdb_Man,rdb_Woman;
    private CheckBox ckb_BastBall,ckb_FootBall;
    private Button btn_Confirm,btn_ReadFile;
    private Button btn_SaveFile1;
    private TextView text_Show;
    private String str="",sex="",hobby="";
    private Button btn_Toast;
    private EditText editText_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();//调用初始化变量
        btn_Confirm.setOnClickListener(this);//按钮点击事件由当前类接口实现接口单击方法
        rdg_MyGroup.setOnCheckedChangeListener(this);
        ckb_FootBall.setOnCheckedChangeListener(this);
        ckb_BastBall.setOnCheckedChangeListener(this);
        btn_Toast.setOnClickListener(this);
        btn_SaveFile1.setOnClickListener(this);
        btn_ReadFile.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){//实现接口
        if(v.getId()==R.id.btn_ReadFile){//读取数据
            try {
                FileInputStream fis=openFileInput("mydata.txt");
                byte[] buffer=new byte[fis.available()];
                fis.read(buffer);//把文件数据读到字节数组中
                String str=new String(buffer);//把数组转化为字符串
                Toast.makeText(this,str,Toast.LENGTH_LONG).show();//显示字符串
                fis.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(v.getId()==R.id.btn_SaveFile1){//保存数据到文件
            try {
                FileOutputStream fos =openFileOutput("mydata.txt",MODE_PRIVATE);
                fos.write((editText_name.getText().toString()+sex+hobby).getBytes());
                fos.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        if(v.getId()==R.id.btn_Confirm)
        {
            text_Show.setText(editText_name.getText()+sex+hobby);
            Toast.makeText(this,sex+hobby,Toast.LENGTH_LONG).show();

        }
        if(v.getId()==R.id.btn_Toast){
            //Toast.makeText(MainActivity.this,"WIFI已断开",Toast.LENGTH_LONG).show();
            AlertDialog dialog;
            AlertDialog.Builder builder=new AlertDialog.Builder(this);
            builder.setTitle("对话框标题");
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setMessage("所选信息:"+sex+hobby+",是否确定？");
            builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    text_Show.setText(sex+hobby);
                    dialog.dismiss();

                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    text_Show.setText("");


                }
            });
            dialog=builder.create();
            dialog.show();

        }
    }
    public void initView()
    {
        //初始化变量
        rdg_MyGroup=findViewById(R.id.rdg_MyRadioGroup);
        rdb_Man=findViewById(R.id.rdb_Man);
        rdb_Woman=findViewById(R.id.rdb_Woman);
        ckb_FootBall=findViewById(R.id.chk_FootBall);
        ckb_BastBall=findViewById(R.id.chk_BastBall);
        btn_Confirm=findViewById(R.id.btn_Confirm);
        text_Show=findViewById(R.id.txt_Show);
        btn_Toast=findViewById(R.id.btn_Toast);
        editText_name=findViewById(R.id.editText_Name);
        btn_SaveFile1=findViewById(R.id.btn_SaveFile1);
        btn_ReadFile=findViewById(R.id.btn_ReadFile);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        //完成单选按钮
        if(checkedId==R.id.rdb_Man){
            sex="男";
            //text_Show.setText("男");

        }
        if(checkedId==R.id.rdb_Woman){
            sex="女";
            // text_Show.setText("女");

        }


    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        //完成复选框
        hobby="";
        if(ckb_BastBall.isChecked()){
            //text_Show.setText("篮球");
            hobby=hobby+"篮球";

        }
        if(ckb_FootBall.isChecked()){
            //text_Show.setText("足球");
            hobby=hobby+"足球";
        }

    }
}