package com.billchen.sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edtName,edtPhone;
    private Button btnAdd,btnQuery,btnDelete,btnUPdate;
    private TextView txtShow;
    private  MyHelper myHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myHelper=new MyHelper(this,"mydb.db",null,1);
        // myHelper.getWritableDatabase();
        //初始化变量
        edtName=findViewById(R.id.et_name);
        edtPhone=findViewById(R.id.et_phone);
        txtShow=findViewById(R.id.tv_show);
        btnAdd=findViewById(R.id.btn_add);
        btnQuery=findViewById(R.id.btn_query);
        btnDelete=findViewById(R.id.btn_delete);
        btnUPdate=findViewById(R.id.btn_update);
        //定义事件
        btnAdd.setOnClickListener(this);
        btnQuery.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnUPdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        SQLiteDatabase db;
        ContentValues cv;
        if(v.getId()==R.id.btn_add)
        {
            db=myHelper.getWritableDatabase();
            String sname=edtName.getText().toString().trim();
            String sphone=edtPhone.getText().toString().trim();
            // db.execSQL("insert into contacts(name,phone) values(?,?)",new String[]{name,phone});
            cv=new ContentValues();
            cv.put("name",sname);
            cv.put("phone",sphone);
            db.insert("contacts",null,cv);
            db.close();
            Toast.makeText(this,"添加成功",Toast.LENGTH_LONG).show();
        }
        if(v.getId()==R.id.btn_delete)
        {
            //删除
            db=myHelper.getWritableDatabase();
            db.delete("contacts",null,null);
            Toast.makeText(this,"删除成功",Toast.LENGTH_LONG);
            db.close();


        }
        if(v.getId()==R.id.btn_update)
        {
            //修改


        }
        if(v.getId()==R.id.btn_query)
        {
            //查询
            String s="";
            db=myHelper.getReadableDatabase();
            Cursor cs=db.query("contacts",null,null,null,null,null,null);
            if(cs.getCount()==0){
                txtShow.setText("没有数据");
            }
            else{
                while(cs.moveToNext()) {
                    //s=s+"姓名："+cs.getString(1)+"电话"+cs.getString(2)+"\n";
                    s = s +"姓名："+ cs.getString(1) + "电话"+cs.getString(2) + "\n";
                    //txtShow.setText(cs.getString(1)+cs.getString(2));
                    Log.i("我的数据：",s);
                    txtShow.setText(s) ;
                }
            }
            cs.close();
            db.close();
        }
    }
}