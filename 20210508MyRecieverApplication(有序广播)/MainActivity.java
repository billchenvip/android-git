package com.example.myrecieverapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
private MyReceiver1 myReceiver1;
private MyReceiver2 myReceiver2;
private  MyReceiver3 myReceiver3;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver1);
        unregisterReceiver(myReceiver2);
        unregisterReceiver(myReceiver3);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter intentFilter1=new IntentFilter();

        intentFilter1.setPriority(1000);
        intentFilter1.addAction("For_Help");
         myReceiver1=new MyReceiver1();
        registerReceiver(myReceiver1,intentFilter1);



        IntentFilter intentFilter2=new IntentFilter();

        intentFilter2.setPriority(900);
        intentFilter2.addAction("For_Help");
        myReceiver2=new MyReceiver2();
        registerReceiver(myReceiver2,intentFilter2);




        IntentFilter intentFilter3=new IntentFilter();

        intentFilter3.setPriority(2000);
        intentFilter3.addAction("For_Help");
        myReceiver3=new MyReceiver3();
        registerReceiver(myReceiver3,intentFilter3);
    }
}
