package com.example.myrecieverapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
private MyReceiver1 myReceiver1;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myReceiver1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter intentFilter=new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.AIRPLANE_MODE");
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        intentFilter.addAction("For_Help");
         myReceiver1=new MyReceiver1();
        registerReceiver(myReceiver1,intentFilter);
    }
}
